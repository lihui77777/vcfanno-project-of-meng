#perl

my $directory = 'D:\work\ASPREE_VCF\annotationDB\DB\Alzheimier\ALZFORUM\web\mutations_pages_v1';

opendir (DIR, $directory) or die $!;

open output, ">mutation_position";

while (my $file = readdir(DIR)){
	
	#print $file."\t";
	
	open file, $directory."\\".$file;
	
	$file =~ s/^v1\_//g;
	print $file."\n";

	
	#print scalar(my @a = <file>);
	
	my $chrom;
	my $start;
	my $end;
	my $wild;
	my $alt;
	
	my @data = <file>;
  if ($data[0] =~ /\, /){
  	$data[0] =~ s/\s+$//g;
  	$data[1] =~ s/\s+$//g;
  	my @mut = split /\, /, $data[0];
  	my @dbsnp = split /\, /, $data[1];
  	my @m1 = split /\s+/, $mut[0];
  	my @m2 = split /\s+/, $mut[1];
  	my @m11 = split /\:/, $m1[0];
  	my @m12 = split /\>/, $m1[1];
  	
  	print output $file."\t".$m11[0]."\t".$m11[1]."\t".$m12[0]."\t".$m12[1]."\t".$dbsnp[0]."\n";
  	
  	my @m21 = split /\:/, $m2[0];
  	my @m22 = split /\>/, $m2[1];
  	
  	print output $file."\t".$m21[0]."\t".$m21[1]."\t".$m22[0]."\t".$m22[1]."\t".$dbsnp[1]."\n";
  	 	
  } else {
  	$data[0] =~ s/\s+$//g;
  	$data[1] =~ s/\s+$//g;
   	my @mut = split /\s+/, $data[0];
  	my @m11 = split /\:/, $mut[0];
  	my @m12 = split /\>/, $mut[1];
  	my @dbsnp =  $data[1];
  	
  	print output $file."\t".$m11[0]."\t".$m11[1]."\t".$m12[0]."\t".$m12[1]."\t".$dbsnp."\n";
  	
  	}


}
