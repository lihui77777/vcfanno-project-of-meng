#perl

open file, "GWAS_Catalog_v2.txt";

open output, ">GWAS_Catalog_v3.txt";

while (my $line = <file> ){
	$line =~ s/\s+$//g;
	my @a = split /\t/, $line;
	my $chr = $a[0];
	my $rsid = $a[1];
	my $riskallel = $a[2];
	my $pvalue = $a[3];
	my $pubmedid = $a[4];
	my $phenotype = $a[5];

    my @schr = split /\;|x/, $chr;
    for (my $i=0; $i<scalar(@schr); $i++){
    	$schr[$i] =~ s/\s+$//g;
    	$schr[$i] =~ s/^\s+//g;
    	$schr[$i] =~ s/^chr//;

        $schr[$i] = "chr".$schr[$i];
    }

    my @srsid = split /\;|x/, $rsid;
    for (my $i=0; $i<scalar(@srsid); $i++){
        $srsid[$i] =~ s/\s+$//g;
    	$srsid[$i] =~ s/^\s+//g;
    	$srsid[$i] =~ s/^rs//;
    }

    my @sriskallel = split /\;|x/, $riskallel;
    for (my $i=0; $i<scalar(@sriskallel); $i++){
        $sriskallel[$i] =~ s/\s+$//g;
    	$sriskallel[$i] =~ s/^\s+//g;
    	#$sriskallel[$i] =~ s/^rs//;
    }

    my $schrnum = scalar(@schr);
    my $srsidnum = scalar(@srsid);
    my $sriskallelnum = scalar(@sriskallel);

    if ( $srsidnum > 0 ){
    	for (my $i=0; $i<$srsidnum; $i++){
    		print output $schr[$i]."\t";
    		print output $srsid[$i]."\t";
    		print output $sriskallel[$i]."\t";
    		print output $pvalue."\t";
    		print output $pubmedid."\t";
    		print output $phenotype."\n";
            my $str1 = $sriskallel[$i];
            my $str2 = $srsid[$i];
    		if ( $str1 =~ /$str1/ ){
            }else {
            	print  "unmatched!\n";
            	print  $str1."\t".$str2."\n";
            }
       	}
    }
    else {
    	print "Error!\n";
    	print $line."\n";
    	exit;
    }
}















